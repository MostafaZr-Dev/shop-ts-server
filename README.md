
#  Shop Project With Typescript
shop project with ** express - typescript - react - nextjs **

**admin-panel**

[panel-repo](https://gitlab.com/MostafaZr-Dev/shop-ts-panel)

**front**

[front-repo](https://gitlab.com/MostafaZr-Dev/shop-ts-front)

##  Technologies

  
### server
- Express
- Multer
- MongoDB(Mongoose)
- Redis
### admin-panel
- react
- react-router
- context-api
- material-ui
- axios
### front
- nextjs
- context-api
- clab-template
  

##  Features

- manage products ( create | update | delete )
- manage categories ( create-category with attributes )
- products list
- shopping cart 
- coupon
- payment ( zarinpal )
- notification ( sms - email )
- settings
