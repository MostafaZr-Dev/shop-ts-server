/* eslint-disable no-unused-vars */
import * as faker from 'faker'
import { max } from 'jalali-moment'

import IProductAttribute from '../../components/products/model/IProductAttribute'
import IAttributeGroup from '../../components/products/model/IAttributeGroup'
import IProduct from '../../components/products/model/IProduct'
import ProductModel from '../../components/products/model/Product'
import { create as CreateCategory } from './CategoryFactory'
import IProductVariation, { IProductVariationItem } from '../../components/products/model/IProductVariation'
import IPriceVariation from '../../components/products/model/IPriceVariation'
import ICategory from '../../components/category/model/ICategory'

faker.setLocale('fa')

const makeProductAttributes = async (count: number = 1) => {
    const attributes: IProductAttribute[] = []
    for (let index = 1; index <= count; index++) {
        const title = faker.random.words(2)
        attributes.push({
            title,
            slug: faker.helpers.slugify(title),
            value: faker.random.words(2),
            filterable: faker.datatype.boolean(),
            hasPrice: faker.datatype.boolean()
        })
    }
    return attributes
}

const makeGroupAttribute = async (count: number = 1) => {
    const attributes: IAttributeGroup[] = []
    for (let index = 1; index <= count; index++) {
        const attributeItems = await makeProductAttributes(faker.datatype.number(15))
        attributes.push({
            title: faker.random.words(2),
            attributes: attributeItems
        })
    }
    return attributes
}
const makeVariationItems = async (count: number = 1) => {
    const variationItems: IProductVariationItem[] = []
    for (let index = 1; index <= count; index++) {
        variationItems.push({
            title: faker.random.word(),
            value: faker.random.word()
        })
    }
    return variationItems
}
const makeVariations = async (count: number = 1) => {
    const variations: IProductVariation[] = []
    for (let index = 1; index <= count; index++) {
        const items = await makeVariationItems(faker.datatype.number(10))
        variations.push({
            title: faker.random.words(2),
            name: faker.random.word(),
            type: faker.random.arrayElement(['color', 'size', 'material']),
            items
        })
    }
    return variations
}
const makePriceVariationItems = async (count: number = 1, variations: IProductVariation[]) => {
    const variationItems: object[] = []
    for (let index = 1; index <= count; index++) {
        const variation = faker.random.arrayElement(variations)
        if (variation) {
            const item = faker.random.arrayElement<IProductVariationItem>(variation.items)
            if (item) {
                variationItems.push({ [variation.name]: item.value })
            }
        }
    }
    return variationItems
}
const makePriceVariations = async (count: number = 1, variations: IProductVariation[]) => {
    const priceVariations: IPriceVariation[] = []

    for (let index = 1; index <= count; index++) {
        const items = await makePriceVariationItems(faker.datatype.number(10), variations)
        priceVariations.push({
            price: faker.commerce.price(undefined, undefined, 0) as unknown as number,
            items
        })
    }
    return priceVariations
}
const buildProduct = async (category: ICategory, attributes: IAttributeGroup[], variations: IProductVariation[], priceVariations: IPriceVariation[]) => {
    const slug = faker.random.words(3)
    return {
        title:faker.commerce.product(),
        slug: faker.helpers.slugify(slug),
        price: faker.commerce.price(undefined, undefined, 0),
        discountedPrice: faker.commerce.price(0, undefined, 0),
        thumbnail: faker.image.abstract(),
        gallery: [faker.image.abstract()],
        category: category._id,
        attributes,
        variations,
        priceVariations,
        stock: faker.datatype.number(100),
        purchasedCount: faker.datatype.number(100),
        commentsCount: faker.datatype.number(100),
        totalScore: faker.datatype.number({ min: 0, max: 5, precision: 0.01 }),
        viewsCount: faker.datatype.number(10000),
    }
}
export async function create(count: number = 1, params?: Partial<IProduct>) {
    const products: IProduct[] = []
    for (let index = 1; index <= count; index++) {
        // create category
        const categories = await CreateCategory(1)
        const attributes = await makeGroupAttribute(faker.datatype.number(15))
        const variations = await makeVariations(faker.datatype.number(10))
        const priceVariations = await makePriceVariations(faker.datatype.number(5), variations)
        const productParams = await buildProduct(categories[0], attributes, variations, priceVariations)
        const finalParams = { ...productParams, ...params }
        const newProduct = new ProductModel(finalParams)
        await newProduct.save()
        products.push(newProduct)
    }
    return products
}
