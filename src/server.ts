import 'module-alias/register'
import 'reflect-metadata'
import * as dotenv from 'dotenv'
import App from "./app";

dotenv.config();

import startMongo from "@infrastructure/connection/mongoose";

startMongo()

const port: number = parseInt(process.env.APP_PORT as string, 10);

const application = new App(port);

application.start();

