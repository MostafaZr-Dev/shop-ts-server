export default [
    {
        name: 'cod',
        title: 'پرداخت درب منزل',
        gateways: null
    },
    {
        name: 'online',
        title: 'آنلاین',
        gateways: [
            {
                name: 'zarinpal',
                title: 'درگاه واسط زرین پال'
            }
        ]
    }
]