import { autoInjectable } from 'tsyringe'

import NotFoundException from "@components/exceptions/NotFoundException";
import User from "@components/users/model/User";
import MailService from "@services/notification/mailer/mailService";
import SMSService from "@services/notification/sms/SMSService";
import IAddOrder from "./IAddOrder";
import IOrder from "./model/IOrder";
import OrderStatus from "./model/orderStatus";
import OrderCompletedMail from "./notifications/email/OrderCompletedMail";
import OrderCompletedSMS from "./notifications/sms/OrderCompletedSMS";
import OrderMongoRepository from "./repositories/OrderMongoRepository";
import StatusVerifier from "./services/statusVerifire";

@autoInjectable()
export default class OrdersService {
    private readonly statusVerifier: StatusVerifier
    private readonly ordersRepository: OrderMongoRepository
    private readonly smsService: SMSService
    private readonly mailService: MailService

    constructor(
        statusVerifier: StatusVerifier,
        ordersRepository: OrderMongoRepository,
        smsService: SMSService,
        mailService: MailService
    ) {
        this.statusVerifier = statusVerifier
        this.ordersRepository = ordersRepository
        this.smsService = smsService
        this.mailService = mailService
    }

    public async updateStatus(orderID: string, newStatus: OrderStatus): Promise<boolean> {
        const order = await this.ordersRepository.findOne(orderID)

        if (!order) {
            throw new NotFoundException('سفارش موردنظر یافت نشد!')
        }

        const canStartTransition: boolean = this.statusVerifier.verify(newStatus, order.status)

        if (canStartTransition) {
            return this.ordersRepository.updateOne({
                _id: orderID
            }, {
                status: newStatus
            })
        }

        return false
    }

    public async addOrder(orderData: IAddOrder): Promise<IOrder | null> {

        const newOrder = await this.ordersRepository.create({
            user: orderData.userID,
            totalPrice: orderData.cart
                .reduce((total, item) => (total + (item.price * item.quantity)), 0),
            finalPrice: orderData.cart
                .reduce((total, item) => (total + (item.discountedPrice * item.quantity)), 0),
            orderLines: orderData.cart.map((item) => ({
                product: item.productID,
                price: item.price,
                discountedPrice: item.discountedPrice,
                quantity: item.quantity,
            })),
            deliveryAddress: orderData.deliveryAddress,
            coupon: orderData.coupon ? orderData.coupon.code : ''
        })

        return newOrder
    }

    public async completeOrder(orderID: string): Promise<void> {

        const order = await this.ordersRepository.findOne(orderID, ['user']) as IOrder

        this.updateStatus(orderID, OrderStatus.PAID)

        const user: any = order.user
        let userMobile = ''
        let userEmail = ''

        if (user instanceof User) {
            userMobile = user.mobile
            userEmail = user.email
        }


        this.smsService.send(new OrderCompletedSMS(userMobile, orderID))
        this.mailService.send(new OrderCompletedMail(userEmail, orderID))

    }

}