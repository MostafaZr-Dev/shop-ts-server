import ICartItem from "@components/cart/ICartItem";

export default interface IAddOrder {
    cart: ICartItem[];
    userID: string;
    coupon: {
        code: string;
        percent: number;
    };
    deliveryAddress: any;
}