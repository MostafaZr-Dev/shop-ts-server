import { NextFunction, Request, Response } from "express";
import { autoInjectable } from 'tsyringe'

import OrderMongoRepository from "../repositories/OrderMongoRepository"
import OrdersTransformer from "./Transformer";
import OrdersService from "../ordersService";

import NotFoundException from "@components/exceptions/NotFoundException";

@autoInjectable()
export default class OrdersController {
    private readonly ordersRepository: OrderMongoRepository
    private readonly ordersTransformer: OrdersTransformer
    private readonly ordersService: OrdersService

    public constructor(
        ordersRepository: OrderMongoRepository,
        ordersTransformer: OrdersTransformer,
        ordersService: OrdersService
    ) {
        this.index = this.index.bind(this)
        this.findOrder = this.findOrder.bind(this)
        this.updateStatus = this.updateStatus.bind(this)

        this.ordersRepository = ordersRepository
        this.ordersTransformer = ordersTransformer
        this.ordersService = ordersService
    }

    public async index(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const page = req.query.page || 1
            const perPage = 8
            const offset: number = (page as unknown as number - 1) * perPage
            
            const orders = await this.ordersRepository.findMany(
                {
                    userData: req.query.q as string,
                },
                ['user'],
                {
                    perPage,
                    offset
                }
            )

            const totalOrders = await this.ordersRepository.findMany({
                userData: req.query.q as string,
            })

            const transformedOrders = this.ordersTransformer.collection(orders)

            res.send({
                orders: transformedOrders,
                _metadata: {
                    page,
                    perPage,
                    totalItems: totalOrders.length,
                    totalPages: Math.ceil(totalOrders.length / perPage),
                }
            })
        } catch (error) {
            next(error)
        }

    }

    public async findOrder(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const { id } = req.params

            const order = await this.ordersRepository.findOne(id, ['user', 'orderLines.product'])

            if (!order) {
                throw new NotFoundException(`سفارش موردنظر یافت نشد`)
            }

            const transformedOrder = this.ordersTransformer.transform(order)

            res.send({
                order: transformedOrder
            })

        } catch (error) {
            next(error)
        }
    }

    public async updateStatus(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const { id } = req.params
            const { orderStatus } = req.body

            const updateResult = await this.ordersService.updateStatus(id, parseInt(orderStatus))


            res.send({
                success: true,
                message: 'عملیات بروزرسانی با موفقیت انجام شد.'
            })


        } catch (error) {
            next(error)
        }
    }




}