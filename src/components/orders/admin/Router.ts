import { Router } from 'express'
import { container } from 'tsyringe'

import OrdersController from './Controller'

const ordersControllerInstance = container.resolve(OrdersController)

const ordersRouter: Router = Router()

ordersRouter.get("/", ordersControllerInstance.index)
ordersRouter.get("/:id", ordersControllerInstance.findOrder)
ordersRouter.put("/:id", ordersControllerInstance.updateStatus)

export default ordersRouter