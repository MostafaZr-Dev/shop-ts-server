import ITransformer from "@components/contracts/ITransformer";
import DateService from "@services/dateService"
import IOrder from "../model/IOrder";

export default class OrdersTransformer implements ITransformer<IOrder>{
    public transform(item: IOrder) {

        return {
            id: item._id,
            user: this.getUser(item.user),
            totalPrice: item.totalPrice,
            finalPrice: item.finalPrice,
            orderLines: item.orderLines,
            deliveryAddress: item.deliveryAddress,
            coupon: this.getCoupon(item.coupon),
            createdAt: DateService.toPersian(item.createdAt.toUTCString()),
            updatedAt: DateService.toPersian(item.updatedAt.toUTCString()),
            status: item.status
        }

    }

    public collection(items: IOrder[]) {
        return items.map(item => this.transform(item))
    }

    private getUser(user: any) {
        if (!user) {
            return null
        }

        return {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email
        }
    }

    private getCoupon(coupon: any) {

        if (!coupon) {
            return null
        }

        return {
            code: coupon.code,
            percent: coupon.percent
        }

    }

}