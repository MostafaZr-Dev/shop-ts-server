import { Document } from "mongoose";

import OrderStatus from "./OrderStatus";
import IAddress from "@components/users/model/IAddress";
import IOrderLine from "./IOrderLine";

export default interface IOrder extends Document {

    user: string;
    totalPrice: number;
    finalPrice: number;
    orderLines: IOrderLine[];
    deliveryAddress: IAddress;
    coupon: string;
    createdAt: Date;
    updatedAt: Date;
    status: OrderStatus;


}