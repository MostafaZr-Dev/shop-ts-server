import { Schema } from "mongoose";

const orderLineSchema: Schema = new Schema({
    product: { type: Schema.Types.ObjectId, ref: "products", required: true },
    price: { type: Number, required: true },
    discountedPrice: { type: Number },
    quantity: { type: Number, required: true },
    createdAt: { type: Date, default: Date.now }
});

export default orderLineSchema;