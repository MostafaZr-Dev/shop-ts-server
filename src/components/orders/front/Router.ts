import { Router } from 'express'
import { container } from 'tsyringe'

import OrdersController from './Controller'
import { auth } from "@middlewares/auth";

const ordersControllerInstance = container.resolve(OrdersController)


const ordersRouter: Router = Router()

ordersRouter.use(auth)

ordersRouter.get("/", ordersControllerInstance.list)
ordersRouter.post("/", ordersControllerInstance.store)

export default ordersRouter