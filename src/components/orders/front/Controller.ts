import { NextFunction, Request, Response } from "express";
import { autoInjectable } from "tsyringe"

import OrderMongoRepository from "../repositories/OrderMongoRepository"
import OrdersService from "../ordersService"
import OrdersTransformer from './Transformer'

import ServerException from "@components/exceptions/ServerException";

@autoInjectable()
export default class OrdersController {
    private readonly ordersRepository: OrderMongoRepository
    private readonly ordersService: OrdersService
    private readonly orderTransformer: OrdersTransformer

    public constructor(
        ordersRepository: OrderMongoRepository,
        ordersService: OrdersService,
        orderTransformer: OrdersTransformer
    ) {
        this.store = this.store.bind(this)
        this.list = this.list.bind(this)

        this.ordersRepository = ordersRepository
        this.ordersService = ordersService
        this.orderTransformer = orderTransformer
    }

    public async store(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {

            const orderData = {
                cart: req.body.cart,
                userID: req.body.userID,
                coupon: req.body.coupon,
                deliveryAddress: req.body.deliveryAddress,
            }

            const newOrder = await this.ordersService.addOrder(orderData)

            if (!newOrder) {
                throw new ServerException('در حال حاضر امکان ثبت سفارش وجود ندارد!')
            }

            res.send({
                success: true,
            })


        } catch (error) {
            next(error)
        }

    }

    public async list(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const { userID } = req.body

            const userOrders = await this.ordersRepository.findMany(
                { user: userID },
                undefined,
                {
                    perPage: 50,
                    offset: 0
                },
                {
                    createdAt: -1
                }
            )

            const transformedOrders = this.orderTransformer.collection(userOrders)


            res.send({
                success: true,
                orders: transformedOrders
            })
        } catch (error) {
            next(error)
        }
    }
}