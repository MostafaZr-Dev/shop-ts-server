import ITransformer from "@components/contracts/ITransformer";
import DateService from "@services/dateService"
import IOrder from "../model/IOrder";

export default class OrdersTransformer implements ITransformer<IOrder>{
    public transform(item: IOrder) {

        return {
            id: item._id,
            totalPrice: item.totalPrice,
            finalPrice: item.finalPrice,
            orderLines: item.orderLines,
            deliveryAddress: item.deliveryAddress,
            createdAt: DateService.toPersian(item.createdAt.toUTCString()),
            updatedAt: DateService.toPersian(item.updatedAt.toUTCString()),
            status: item.status
        }

    }

    public collection(items: IOrder[]) {
        return items.map(item => this.transform(item))
    }

}