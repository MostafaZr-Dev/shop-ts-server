import IPagination from "@components/contracts/IPagination";
import IRepository from "@components/contracts/IRepository";
import IUser from "@components/users/model/IUser";
import IOrder from "../model/IOrder";
import OrderStatus from "../model/OrderStatus";


export default interface IOrderRepository extends IRepository<IOrder> {

    findByStatus(status: OrderStatus): Promise<IOrder[]>

}