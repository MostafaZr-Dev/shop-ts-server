import { FilterQuery, Types } from "mongoose";
import { autoInjectable } from "tsyringe";

import IOrder from "../model/IOrder";
import OrderStatus from "../model/OrderStatus";
import IOrderRepository from "./IOrderRepository";
import OrderModel from "../model/Order"
import IPagination from "@components/contracts/IPagination";
import UserMongoRepository from "@components/users/repositories/UserMongoRepository";
import ObjectInterface from "@components/contracts/ObjectInterface";

@autoInjectable()
export default class OrderMongoRepository implements IOrderRepository {

    private readonly usersRepository: UserMongoRepository

    constructor(
        usersRepository: UserMongoRepository
    ) {
        this.usersRepository = usersRepository
    }

    public async findByStatus(status: OrderStatus): Promise<IOrder[]> {
        return OrderModel.find({ status })
    }

    public async findOne(ID: string, relations?: string[]): Promise<IOrder | null> {
        const orderQuery = OrderModel.findById(ID)

        if (relations) {
            relations.forEach((relation: string) => {
                orderQuery.populate(relation)
            })
        }

        return orderQuery.exec()
    }

    public async findMany(params: ObjectInterface, relations?: string[], pagination?: IPagination, sort?: any): Promise<IOrder[]> {
        const orderQueryParams: ObjectInterface = {}

        if (params.userData) {
            const users = await this.usersRepository.findMany({
                $or: [
                    { firstName: { $regex: params.userData } },
                    { lastName: { $regex: params.userData } },
                    { email: { $regex: params.userData } },
                ]
            })

            orderQueryParams.user = {
                $in: users.map((user => user._id))
            }
        }

        if (params.user) {
            const objectID = Types.ObjectId
            orderQueryParams.user = new objectID(params.user)
        }

        const orderQuery = OrderModel.find(orderQueryParams)

        if (sort) {
            orderQuery.sort(sort)
        }

        if (relations && relations.length > 0) {

            relations.forEach((relation: string) => {
                orderQuery.populate(relation)
            })

        }

        if (pagination) {
            orderQuery.limit(pagination.perPage).skip(pagination.offset)
        }

        return orderQuery.exec()
    }

    public async create(params: any): Promise<IOrder> {
        const newOrder = new OrderModel({
            ...params
        })

        return newOrder.save()
    }

    public async updateOne(where: Partial<IOrder>, updateData: Partial<IOrder>): Promise<boolean> {
        const updateResult = await OrderModel.updateOne(where as FilterQuery<IOrder>, updateData)

        if (updateResult.nModified > 0) {
            return true
        }

        return false

    }

    public async updateMany(where: Partial<IOrder>, updateData: Partial<IOrder>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(ID: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async count(params: any): Promise<number> {
        return new Promise((resolve, reject) => resolve(2))
    }

}