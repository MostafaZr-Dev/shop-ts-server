import OrderStatus from "@components/orders/model/OrderStatus";
import CanceledToDelivered from "./handlers/CanceledToDelivered";
import CanceledToRefunded from "./handlers/CanceledToRefunded";
import InitToDelivered from "./handlers/InitToDelivered";
import InitToRefunded from "./handlers/InitToRefunded";


export default class StatusVerifier {

    public verify(newStatus: OrderStatus, currentStatus: OrderStatus) {

        const canceledToRefunded: CanceledToRefunded = new CanceledToRefunded()
        const canceledToDelivered: CanceledToDelivered = new CanceledToDelivered(canceledToRefunded)
        const initToDelivered: InitToDelivered = new InitToDelivered(canceledToDelivered)
        const initToRefunded: InitToRefunded = new InitToRefunded(initToDelivered)

        return initToRefunded.handle(newStatus, currentStatus)
    }

}