import ValidationException from "@components/exceptions/ValidationException";
import OrderStatus from "@components/orders/model/OrderStatus";
import Handler from "../handler";

class InitToRefunded extends Handler {


    public process(newStatus: OrderStatus, currentStatus: OrderStatus): boolean {

        if (currentStatus === OrderStatus.INIT && newStatus === OrderStatus.REFUNDED) {
            throw new ValidationException('تغییر وضعیت سفارش از ثبت شده به مرجوع شده امکان پذیر نیست!')
        }

        return true

    }

}

export default InitToRefunded