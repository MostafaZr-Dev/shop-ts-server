import ValidationException from "@components/exceptions/ValidationException";
import OrderStatus from "@components/orders/model/OrderStatus";
import Handler from "../handler";

export default class InitToDelivered extends Handler {


    public process(newStatus: OrderStatus, currentStatus: OrderStatus): boolean {

        if (currentStatus === OrderStatus.INIT && newStatus === OrderStatus.DELIVERED) {
            throw new ValidationException('تغییر وضعیت سفارش از ثبت شده به تحویل داده شده امکان پذیر نیست!')
        }

        return true

    }

}