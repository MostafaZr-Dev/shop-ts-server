import ValidationException from "@components/exceptions/ValidationException";
import OrderStatus from "@components/orders/model/OrderStatus";
import Handler from "../handler";

export default class CanceledToRefunded extends Handler {

    
    public process(newStatus: OrderStatus, currentStatus: OrderStatus): boolean {

        if (currentStatus === OrderStatus.CANCELED && newStatus === OrderStatus.REFUNDED) {
            throw new ValidationException('تغییر وضعیت سفارش از لغو شده به مرجوع شده امکان پذیر نیست!')
        }

        return true

    }

}