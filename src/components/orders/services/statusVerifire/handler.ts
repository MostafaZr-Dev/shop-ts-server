import OrderStatus from "@components/orders/model/OrderStatus"

export default abstract class Handler {

    private successor?: Handler | null = null

    constructor(successor: Handler | null = null) {
        this.successor = successor
    }

    public handle(newStatus: OrderStatus, currentStatus: OrderStatus): boolean {
        const result = this.process(newStatus, currentStatus)

        if (result && this.successor) {
            return this.successor.handle(newStatus, currentStatus)
        }

        return result
    }

    protected abstract process(newStatus: OrderStatus, currentStatus: OrderStatus): boolean

}