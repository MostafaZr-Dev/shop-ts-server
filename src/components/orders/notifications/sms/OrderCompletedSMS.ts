import ISMSMessage from "@services/notification/sms/contracts/ISMSMessage"


class OrderCompletedSMS implements ISMSMessage {
    public readonly to: string
    public readonly message: string

    constructor(to: string, orderID: string) {
        this.to = to
        this.message = `کاربر گرامی سفارش شما با شناسه ${orderID} موفقیت پرداخت شد`
    }

}

export default OrderCompletedSMS