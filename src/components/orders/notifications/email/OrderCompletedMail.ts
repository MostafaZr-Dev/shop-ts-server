import IMailMessage from "@services/notification/mailer/contracts/IMailMessage"


class OrderCompletedMail implements IMailMessage {
    public readonly receipent: string
    public readonly body: string
    public readonly subject: string

    constructor(receipent: string, orderID: string) {
        this.receipent = receipent
        this.subject = 'تایید پرداخت سفارش'
        this.body = `کاربر گرامی سفارش شما با شناسه ${orderID} موفقیت پرداخت شد`
    }

}

export default OrderCompletedMail