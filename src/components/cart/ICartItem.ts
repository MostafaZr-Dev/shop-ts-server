export default interface ICartItem {
    quantity: number;
    discountedPrice: number;
    price: number;
    productID: string;
    productThumbnail: string;
    productTitle: string;
}