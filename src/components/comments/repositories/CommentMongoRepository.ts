
import IComment from "../model/IComment"
import CommentModel from "../model/Comment"
import ICommentRepository from "./ICommentRepository";



export default class CommentMongoRepository implements ICommentRepository {

    public async findByProductID(productID: string, relations?: string[]): Promise<IComment[]> {
        const commentQuery = CommentModel.find({
            product: productID
        })

        if (relations) {
            relations.forEach((relation: string) => {
                commentQuery.populate(relation)
            })
        }

        return commentQuery.exec()
    }

    public async findOne(ID: string): Promise<IComment | null> {
        return CommentModel.findById(ID)
    }

    public async findMany(params: any): Promise<IComment[]> {
        return CommentModel.find(params)
    }

    public async create(params: any): Promise<IComment> {
        const newProduct = new CommentModel({
            ...params
        })

        return newProduct.save()
    }

    public async updateOne(where: Partial<IComment>, updateData: Partial<IComment>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async updateMany(where: Partial<IComment>, updateData: Partial<IComment>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(ID: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

}