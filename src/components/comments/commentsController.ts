import { Request, Response } from "express"
import CommentMongoRepository from "./repositories/CommentMongoRepository"
import ICommentRepository from "./repositories/ICommentRepository"



export default class commentsController {

    private readonly commentsRepository: ICommentRepository

    public constructor() {
        this.index = this.index.bind(this)

        this.commentsRepository = new CommentMongoRepository()
    }

    public async index(req: Request, res: Response): Promise<void> {
        
    }

}