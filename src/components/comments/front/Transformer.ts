import ITransformer from "@components/contracts/ITransformer";
import DateService from "@services/dateService"
import IComment from "../model/IComment"
import { buildAvatar } from '@services/avatarService'

export default class CommentsTransformer implements ITransformer<IComment>{
    public transform(item: IComment) {

        return {
            id: item._id,
            title: item.title,
            user: this.getUser(item.user),
            product: item.product,
            body: item.body,
            isBuyer: item.isBuyer,
            adviceToBuy: item.adviceToBuy,
            createdAt: DateService.toPersian(item.createdAt.toUTCString()),
            status: item.status
        }

    }

    public collection(items: IComment[]) {
        return items.map(item => this.transform(item))
    }

    public getUser(user: any) {
        return {
            firstName: user.firstName,
            lastName: user.lastName,
            avatar: buildAvatar(user.email)
        }
    }

    public getProduct(product: any) {
        return {
            id: product._id,
            title: product.title
        }
    }

}