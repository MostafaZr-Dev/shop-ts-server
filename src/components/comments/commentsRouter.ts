import { Router } from "express"

import CommentsController from "./commentsController"

const commentsControllerInstance = new CommentsController()

const commentsRouter = Router()

commentsRouter.get('/', commentsControllerInstance.index)


export default commentsRouter