import { Schema, model } from "mongoose";
import AdviceToBuy from "./AdviceToBuy";
import CommentStatus from "./CommentStatus";

import IComment from "./IComment";

const commentSchema: Schema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: "users" },
    product: { type: Schema.Types.ObjectId, ref: "products" },
    title: { type: String, ref: "products", required: true },
    body: { type: String, ref: "products", required: true },
    createdAt: { type: Date, default: Date.now },
    isBuyer: { type: Boolean, default: false },
    adviceToBuy: { type: AdviceToBuy, default: AdviceToBuy.NOT_SURE },
    status: { type: CommentStatus, default: CommentStatus.PENDING },
});


export default model<IComment>("comments", commentSchema)