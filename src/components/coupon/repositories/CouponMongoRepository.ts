import CouponModel from "../model/Coupon";
import ICoupon from "../model/ICoupon";
import ICouponRepository from "./ICouponRepository";



export default class CouponMongoRepository implements ICouponRepository {


    public async findByCode(code: string): Promise<ICoupon | null> {
        return CouponModel.findOne({ code })
    }

    public async findOne(id: string): Promise<ICoupon | null> {
        throw new Error("Method not implemented.");
    }

    public async findMany(params: any): Promise<ICoupon[]> {
        return CouponModel.find(params)
    }

    public async create(params: any): Promise<ICoupon> {
        const newCoupon = new CouponModel({ ...params })

        return newCoupon.save()
    }

    public async updateOne(where: Partial<ICoupon>, updateData: Partial<ICoupon>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async updateMany(where: Partial<ICoupon>, updateData: Partial<ICoupon>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(ID: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

}