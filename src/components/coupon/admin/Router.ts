import { Router } from "express"
import { container } from 'tsyringe'

import CouponsController from "./Controller"

const couponsControllerInstance = container.resolve(CouponsController)

const couponsRouter = Router()

couponsRouter.get('/', couponsControllerInstance.index)
couponsRouter.post('/', couponsControllerInstance.store)


export default couponsRouter