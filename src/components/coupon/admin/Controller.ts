import { NextFunction, Request, Response } from "express";
import { autoInjectable } from 'tsyringe'

import CouponsTransformer from "./Transformer";
import CouponMongoRepository from "../repositories/CouponMongoRepository";

@autoInjectable()
export default class CouponsController {

    private readonly couponsRepository: CouponMongoRepository
    private readonly couponsTransformer: CouponsTransformer

    public constructor(
        couponsRepository: CouponMongoRepository,
        couponsTransformer: CouponsTransformer
    ) {
        this.index = this.index.bind(this)
        this.store = this.store.bind(this)

        this.couponsRepository = couponsRepository
        this.couponsTransformer = couponsTransformer
    }

    public async index(req: Request, res: Response): Promise<void> {
        const coupons = await this.couponsRepository.findMany({})

        const transformedCoupons = this.couponsTransformer.collection(coupons)

        res.send({
            coupons: transformedCoupons
        })
    }

    public async store(req: Request, res: Response, next: NextFunction): Promise<void> {

        try {
            const couponData = {
                code: req.body.code,
                percent: req.body.percent,
                limit: req.body.limit,
                expiresAt: req.body.expiresAt,
                constraints: req.body.constraints
            }

            const newCoupon = await this.couponsRepository.create(couponData)

            if (newCoupon) {
                res.send({
                    success: true,
                    message: 'کد تخفیف با موفقیت ایجاد شد'
                })
            }
        } catch (error) {
            next(error)
        }

    }

}