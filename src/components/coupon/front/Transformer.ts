import ITransformer from "@components/contracts/ITransformer";
import DateService from "@services/dateService"
import ICoupon from "../model/ICoupon";

export default class CouponsTransformer implements ITransformer<ICoupon>{
    public transform(item: ICoupon) {

        return {
            id: item._id,
            code: item.code,
            percent: item.percent,
            limit: item.limit,
            used: item.used,
            expiresAt: item.expiresAt ? DateService.toPersian(item.expiresAt.toUTCString()) : '',
            constraints: item.constraints,
            status: item.status
        }

    }

    public collection(items: ICoupon[]) {
        return items.map(item => this.transform(item))
    }

}