import { NextFunction, Request, Response } from "express";
import { autoInjectable } from 'tsyringe'

import CouponsTransformer from "./Transformer";
import CouponMongoRepository from "../repositories/CouponMongoRepository";
import CouponValidator from "@services/coupon/couponValidator/CouponValidator";

import ValidationException from '@components/exceptions/ValidationException'
import NotFoundException from '@components/exceptions/NotFoundException'

@autoInjectable()
export default class CouponsController {

    private readonly couponsRepository: CouponMongoRepository
    private readonly couponsTransformer: CouponsTransformer

    public constructor(
        couponsRepository: CouponMongoRepository,
        couponsTransformer: CouponsTransformer
    ) {
        this.validateCoupon = this.validateCoupon.bind(this)

        this.couponsRepository = couponsRepository
        this.couponsTransformer = couponsTransformer
    }

    public async validateCoupon(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {

            const { couponCode } = req.body

            if (!couponCode) {
                throw new ValidationException('کد تخفیف معتبر نمی باشد!')
            }

            const couponValidator = new CouponValidator()

            const coupon = await this.couponsRepository.findByCode(couponCode)

            if (!coupon) {
                throw new NotFoundException('کد تخفیف معتبر نمی باشد!')
            }

            couponValidator.handle(null, coupon)

            res.send({
                success: true,
                message: 'کد تخفیف با موفقیت اعمال شد',
                coupon: {
                    code: coupon.code,
                    percent: coupon.percent
                }
            })

        } catch (error) {
            next(error)
        }
    }

}