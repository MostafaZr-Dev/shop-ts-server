import { Router } from "express"
import { container } from 'tsyringe'

import CouponsController from "./Controller"

const couponsControllerInstance = container.resolve(CouponsController)

const couponsRouter = Router()

couponsRouter.post('/validation', couponsControllerInstance.validateCoupon)

export default couponsRouter