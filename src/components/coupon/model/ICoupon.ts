import { Document } from "mongoose";

import CouponStatus from "./couponStatus";

type ConstraintsType = {
    users: string[],
    products: string[]
}

export default interface ICoupon extends Document {
    code: string;
    percent: number;
    limit: number;
    used: number;
    expiresAt: Date;
    constraints: ConstraintsType;
    status: CouponStatus;
}