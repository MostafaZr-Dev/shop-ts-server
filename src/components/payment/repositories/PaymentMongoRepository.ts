import IPayment from "../model/IPayment";
import PaymentStatus from "../model/paymentStatus";
import IPaymentRepository from "./IPaymentRepository";
import PaymentModel from "../model/Payment"
import IPagination from "@components/contracts/IPagination";
import ObjectInterface from "@components/contracts/ObjectInterface";
import UserMongoRepository from "@components/users/repositories/UserMongoRepository";
import IUser from "@components/users/model/IUser";
import IUserRepository from "@components/users/repositories/IUserRepository";


export default class PaymentMongoRepository implements IPaymentRepository {

    private readonly usersRepository: IUserRepository

    constructor() {
        this.usersRepository = new UserMongoRepository()
    }

    public async findByStatus(status: PaymentStatus): Promise<IPayment[]> {
        return PaymentModel.find({ status })
    }

    public async findByReserve(reserve: string): Promise<IPayment | null> {
        return PaymentModel.findOne({ reserve })
    }

    public async findOne(id: string, relations?: string[]): Promise<IPayment | null> {
        return PaymentModel.findById(id)
    }

    public async findMany(params: ObjectInterface, relations?: string[], pagination?: IPagination): Promise<IPayment[]> {
        const paymentQueryParams: ObjectInterface = {}

        if (params.user) {
            const users = await this.usersRepository.findMany({
                $or: [
                    { firstName: { $regex: params.user } },
                    { lastName: { $regex: params.user } },
                    { email: { $regex: params.user } },
                ]
            })

            paymentQueryParams.user = {
                $in: users.map(((user: IUser) => user._id))
            }
        }

        const paymentQuery = PaymentModel.find(paymentQueryParams)

        if (relations && relations.length > 0) {

            relations.forEach((relation: string) => {
                paymentQuery.populate(relation)
            })

        }

        if (pagination) {
            paymentQuery.limit(pagination.perPage).skip(pagination.offset)
        }

        return paymentQuery.exec()
    }

    public async create(params: any): Promise<IPayment> {
        const newProduct = new PaymentModel({
            ...params
        })

        return newProduct.save()
    }

    public async updateOne(where: Partial<IPayment>, updateData: Partial<IPayment>): Promise<boolean> {
        // return PaymentModel.updateOne(where, updateData)
        throw new Error("Method not implemented.");
    }

    public async updateMany(where: Partial<IPayment>, updateData: Partial<IPayment>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(ID: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

}