

import IRepository from "@components/contracts/IRepository";
import IPayment from "../model/IPayment";
import PaymentStatus from "../model/paymentStatus";


export default interface IPaymentRepository extends IRepository<IPayment> {

    findByStatus(status: PaymentStatus): Promise<IPayment[]>
    findByReserve(reserve: string): Promise<IPayment | null>

}