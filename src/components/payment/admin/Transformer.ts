import ITransformer from "@components/contracts/ITransformer";
import DateService from "@services/dateService"
import IPayment from "../model/IPayment";

export default class PaymentsTransformer implements ITransformer<IPayment>{
    public transform(item: IPayment) {

        return {
            id: item._id,
            user: this.getUser(item.user),
            order: this.getOrder(item.order),
            amount: item.amount,
            method: item.method,
            reserve: item.reserve,
            reference: item.reference,
            createdAt: DateService.toPersian(item.createdAt.toUTCString()),
            updatedAt: DateService.toPersian(item.updatedAt.toUTCString()),
            status: item.status
        }

    }

    public collection(items: IPayment[]) {
        return items.map(item => this.transform(item))
    }

    private getUser(user: any) {
        if (!user) {
            return null
        }

        return {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email
        }
    }

    private getOrder(order: any) {
        return {
            id: order._id,
        }
    }

}