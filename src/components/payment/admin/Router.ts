import { Router } from 'express'
import { container } from 'tsyringe'

import PaymentsController from './Controller'

const paymentsControllerInstance = container.resolve(PaymentsController)

const paymentsRouter: Router = Router()

paymentsRouter.get("/", paymentsControllerInstance.index)

export default paymentsRouter