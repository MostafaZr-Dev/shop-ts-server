import { NextFunction, Request, Response } from "express";
import { autoInjectable } from 'tsyringe'

import IPaymentRepository from "../repositories/IPaymentRepository";
import PaymentMongoRepository from "../repositories/PaymentMongoRepository";
import PaymentsTransformer from './Transformer'

@autoInjectable()
export default class PaymentController {

    private readonly paymentsRepository: IPaymentRepository
    private readonly paymentsTransformer: PaymentsTransformer

    public constructor(
        paymentsRepository: PaymentMongoRepository,
        paymentsTransformer: PaymentsTransformer
    ) {
        this.index = this.index.bind(this)

        this.paymentsRepository = paymentsRepository
        this.paymentsTransformer = paymentsTransformer
    }

    public async index(req: Request, res: Response, next: NextFunction): Promise<void> {

        try {
            const page = req.query.page || 1
            const perPage = 8
            const offset: number = (page as unknown as number - 1) * perPage

            const payments = await this.paymentsRepository.findMany(
                {},
                ['user', 'order'],
                {
                    perPage,
                    offset
                }
            )

            const totalPayments = await this.paymentsRepository.findMany({
                user: req.query.q as string,
            })

            const transformedPayments = this.paymentsTransformer.collection(payments)

            res.send({
                payments: transformedPayments,
                _metadata: {
                    page,
                    perPage,
                    totalItems: totalPayments.length,
                    totalPages: Math.ceil(totalPayments.length / perPage),
                }
            })
        } catch (error) {
            next(error)
        }

    }


}