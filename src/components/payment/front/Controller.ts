import { NextFunction, Request, Response } from "express";
import IPaymentRepository from "../repositories/IPaymentRepository";
import PaymentMongoRepository from "../repositories/PaymentMongoRepository";
import PaymentsTransformer from './Transformer'
import MethodsList from '@config/paymentMethods'

export default class PaymentController {

    private readonly paymentsRepository: IPaymentRepository
    private readonly paymentsTransformer: PaymentsTransformer

    public constructor() {
        this.methodsList = this.methodsList.bind(this)

        this.paymentsRepository = new PaymentMongoRepository()
        this.paymentsTransformer = new PaymentsTransformer()
    }

    public async methodsList(req: Request, res: Response, next: NextFunction): Promise<void> {

        try {
            res.send({
                success: true,
                methods: MethodsList
            })
        } catch (error) {
            res.send({
                success: false,
                message: 'لیست درگاه ها در حال حاضر قابل دریافت نیست!'
            })
        }

    }


}