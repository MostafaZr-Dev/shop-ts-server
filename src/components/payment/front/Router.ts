import { Router } from 'express'

import PaymentsController from './Controller'

const paymentsControllerInstance = new PaymentsController()

const paymentsRouter: Router = Router()

paymentsRouter.get("/methods", paymentsControllerInstance.methodsList)

export default paymentsRouter