import { Document } from "mongoose";

import PaymentStatus from "./paymentStatus";

export default interface IPayment extends Document {

    user: string;
    order: string;
    amount: number;
    method: string;
    gateway: string;
    reserve: string;
    reference: string;
    createdAt: Date;
    updatedAt: Date;
    status: PaymentStatus

}