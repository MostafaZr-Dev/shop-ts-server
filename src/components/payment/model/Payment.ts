import { Schema, model } from "mongoose";

import IPayment from "./IPayment";
import PaymentStatus from "./paymentStatus";

const paymentSchema: Schema = new Schema({

    user: { type: Schema.Types.ObjectId, ref: "users" },
    order: { type: Schema.Types.ObjectId, ref: "orders" },
    amount: { type: Number, required: true },
    method: { type: String, required: true },
    gateway: { type: String, required: true },
    reserve: { type: String, required: true },
    reference: { type: String, default: null },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    status: { type: PaymentStatus, required: true, default: PaymentStatus.PENDING }

});

paymentSchema.set('toJSON', {
    virtuals: true
})

export default model<IPayment>("payments", paymentSchema)