import ITransformer from "@components/contracts/ITransformer";
import ICategory from "../model/ICategory";

export default class CategoryTransformer implements ITransformer<ICategory>{
    public transform(item: ICategory) {

        return {
            id: item._id,
            title: item.title,
            slug: item.slug,
            groups: item.groups,
        }

    }

    public collection(items: ICategory[]) {
        return items.map(item => this.transform(item))
    }

}