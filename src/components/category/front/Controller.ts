import { NextFunction, Request, Response } from "express"
import * as _ from 'lodash'

import CategoryMongoRepository from "../repositories/CategoryMongoRepository"
import ICategoryRepository from "../repositories/ICategoryRepository"
import CategoryTransformer from "./Transformer"
import NotFoundException from "@components/exceptions/NotFoundException"
import ProductMongoRepository from "@components/products/repositories/ProductMongoRepository"
import ProductsTransformer from "@components/products/front/Transformer"

class CategoryController {

    private readonly categoryRepository: ICategoryRepository
    private readonly categoryTransformer: CategoryTransformer

    constructor() {
        this.list = this.list.bind(this)
        this.productsCategory = this.productsCategory.bind(this)

        this.categoryRepository = new CategoryMongoRepository()
        this.categoryTransformer = new CategoryTransformer()
    }

    public async list(req: Request, res: Response, next: NextFunction) {

        try {
            const categories = await this.categoryRepository.findMany(
                {},
                undefined,
                {
                    perPage: 40,
                    offset: 0
                }
            )

            res.send({
                success: true,
                categories: this.categoryTransformer.collection(categories)
            })
        } catch (error) {
            next(error)
        }
    }

    public async productsCategory(req: Request, res: Response, next: NextFunction) {
        try {
            const { slug } = req.params

            const category = await this.categoryRepository.findBySlug(slug)

            if (!category) {
                throw new NotFoundException('دسته بندی موردنظر یافت نشد!')
            }

            const productsQuery: any = {
                category: category._id
            }

            const rawQuery = req.query

            delete rawQuery.slug

            if (_.size(rawQuery) > 0) {
                const titles: string[] = []
                const slugs: string[] = []

                _.forEach(rawQuery, (value, key) => {
                    const values = value as string

                    titles.push(key)

                    if (values.includes(",")) {
                        slugs.push(...values.split(','))
                    } else {
                        slugs.push(values)
                    }
                })

                productsQuery['attributes.title'] = {
                    $in: titles
                }
                
                productsQuery['attributes.attributes.slug'] = {
                    $in: slugs
                }
            }

            const productsRepository = new ProductMongoRepository()
            const productsTransformer = new ProductsTransformer()

            const products = await productsRepository.findMany(
                productsQuery
            );

            res.send({
                success: true,
                products: productsTransformer.collection(products),
                category: this.categoryTransformer.transform(category)
            })

        } catch (error) {
            next(error)
        }
    }

}

export default CategoryController