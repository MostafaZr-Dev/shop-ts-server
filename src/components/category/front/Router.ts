import { Router } from "express"

import CategoryController from "./Controller"

const categoryControllerInstance = new CategoryController()
const router: Router = Router()

router.get('/', categoryControllerInstance.list)
router.get('/:slug/products', categoryControllerInstance.productsCategory)

export default router

