import IPagination from "@components/contracts/IPagination";
import ICategory from "../model/ICategory";
import CategoryModel from "../model/Category";
import ICategoryRepository from "./ICategoryRepository";
import ObjectInterface from "@components/contracts/ObjectInterface";


export default class CategoryMongoRepository implements ICategoryRepository {

    public async findBySlug(slug: string): Promise<ICategory | null> {
        return CategoryModel.findOne({
            slug
        })
    }

    public async findOne(id: string, relations?: string[]): Promise<ICategory | null> {
        return CategoryModel.findById(id)
    }

    public async findMany(params: any, relations?: string[], pagination?: IPagination, sort?: any): Promise<ICategory[]> {
        const categoryQueryParams: ObjectInterface = {}

        const categoryQuery = CategoryModel.find(categoryQueryParams)

        if (sort) {
            categoryQuery.sort(sort)
        }

        if (relations) {
            relations.forEach((relation: string) => {
                categoryQuery.populate(relation)
            })
        }

        if (pagination) {
            categoryQuery.limit(pagination.perPage).skip(pagination.offset)
        }

        return categoryQuery.exec()
    }

    public async create(params: any): Promise<ICategory> {
        const newCategory = new CategoryModel({
            ...params
        })

        return newCategory.save()
    }

    public async updateOne(where: Partial<ICategory>, updateData: Partial<ICategory>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async updateMany(where: Partial<ICategory>, updateData: Partial<ICategory>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(ID: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

}