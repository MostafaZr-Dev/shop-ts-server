export default interface IAttribute {
    title: string;
    slug: string;
    fliterable: boolean;
    hasPrice: boolean;
}