import { Request, Response } from "express"

import CategoryModel from "../model/Category"
import CategoryMongoRepository from "../repositories/CategoryMongoRepository"
import ICategoryRepository from "../repositories/ICategoryRepository"

class CategoryController {
    private readonly categoryRepository: ICategoryRepository

    constructor() {
        this.store = this.store.bind(this)
        this.list = this.list.bind(this)

        this.categoryRepository = new CategoryMongoRepository()
    }

    public async store(req: Request, res: Response) {

        const newCategory = await this.categoryRepository.create({
            ...req.body
        })

        return res.send({ success: true })
    }


    public async list(req: Request, res: Response) {

        const categories = await CategoryModel.find({}, { title: 1, slug: 1 }).exec()

        return res.send({ categories })
    }

    public async attributes(req: Request, res: Response) {

        const { id: categoryID } = req.params

        const categories = await CategoryModel.findById(categoryID)

        let attributes: object[] = []
        
        if (categories) {
            attributes = categories.groups.map(group => {
                return {
                    title: group.title,
                    attributes: group.attributes
                }
            })
        }

        return res.send({ attributes })

    }

}

export default CategoryController