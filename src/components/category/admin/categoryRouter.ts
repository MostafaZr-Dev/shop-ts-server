import { Router } from "express"

import CategoryController from "./categoryController"

const categoryControllerInstance = new CategoryController()
const router: Router = Router()

router.post('/', categoryControllerInstance.store)
router.get('/', categoryControllerInstance.list)
router.get('/:id/attributes', categoryControllerInstance.attributes)

export default router

