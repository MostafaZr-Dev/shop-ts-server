import { NextFunction, Request, Response } from "express";
import { autoInjectable } from 'tsyringe'

import OrdersService from "@components/orders/ordersService";
import PaymentService from "@services/payment/PaymentService";

import ServerException from "@components/exceptions/ServerException";
import PaymentMongoRepository from "@components/payment/repositories/PaymentMongoRepository";

@autoInjectable()
export default class OrdersController {
    private readonly ordersService: OrdersService
    private readonly paymentService: PaymentService
    private readonly paymentRepository: PaymentMongoRepository

    public constructor(
        ordersService: OrdersService,
        paymentService: PaymentService,
        paymentRepository: PaymentMongoRepository
    ) {
        this.purchaseOrder = this.purchaseOrder.bind(this)
        this.verifyPayment = this.verifyPayment.bind(this)

        this.ordersService = ordersService
        this.paymentService = paymentService
        this.paymentRepository = paymentRepository
    }

    public async purchaseOrder(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {

            const orderData = {
                cart: req.body.cart,
                userID: req.body.userID,
                coupon: req.body.coupon,
                deliveryAddress: req.body.deliveryAddress,
            }

            const newOrder = await this.ordersService.addOrder(orderData)

            if (!newOrder) {
                throw new ServerException('در حال حاضر امکان ثبت سفارش وجود ندارد!')
            }

            const paymentResult = await this.paymentService.payOrder(newOrder, req.body.method, req.body.gateway)

            if (paymentResult.success) {
                res.send({
                    success: true,
                    url: paymentResult.url
                })
            }




        } catch (error) {
            next(error)
        }

    }

    public async verifyPayment(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {

            const { userID, reserve, authority, status } = req.body

            const paymentData = {
                authority,
                status,
                reserve
            }

            const paymentVerifyResult = await this.paymentService.verifyPayment(paymentData)

            if (paymentVerifyResult.success) {

                const payment = await this.paymentRepository.findByReserve(paymentData.reserve)


                this.ordersService.completeOrder(payment?.order as string)

                
            }

            res.send(paymentVerifyResult)


        } catch (error) {
            next(error)
        }

    }
}