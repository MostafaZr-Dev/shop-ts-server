import { Router } from 'express'
import { container } from 'tsyringe'

import PurchaseController from './Controller'
import { auth } from "@middlewares/auth";

const purchaseControllerInstance = container.resolve(PurchaseController)

const purchaseRouter: Router = Router()

purchaseRouter.use(auth)

purchaseRouter.post("/", purchaseControllerInstance.purchaseOrder)
purchaseRouter.post("/verification", purchaseControllerInstance.verifyPayment)

export default purchaseRouter