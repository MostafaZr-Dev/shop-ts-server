import { Router } from "express";


import AuthController from "./authController";

const authControllerInstance = new AuthController();

const authRouter: Router = Router();

authRouter.get("/check", authControllerInstance.check);
authRouter.post("/login", authControllerInstance.authenticate);
authRouter.post("/register", authControllerInstance.register);


export default authRouter;