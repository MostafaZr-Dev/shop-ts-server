import { NextFunction, Request, Response } from "express";

import AuthService from "@services/authService";
import NotFoundException from "@components/exceptions/NotFoundException";
import ServerException from "@components/exceptions/ServerException";
import TokenService from "@services/tokenService";
import IUser from "@components/users/model/IUser";
import ValidationException from "@components/exceptions/ValidationException";
import { JwtPayload } from "jsonwebtoken";
import IUserRepository from "@components/users/repositories/IUserRepository";
import UserMongoRepository from "@components/users/repositories/UserMongoRepository";
import UsersTransformer from "@components/users/front/Transformer";


export default class AuthController {
    private readonly authService: AuthService
    private readonly usersRepository: IUserRepository
    private readonly usersTransformer: UsersTransformer

    constructor() {
        this.authenticate = this.authenticate.bind(this)
        this.register = this.register.bind(this)
        this.check = this.check.bind(this)

        this.authService = new AuthService()
        this.usersRepository = new UserMongoRepository()
        this.usersTransformer = new UsersTransformer()
    }


    public async authenticate(req: Request, res: Response, next: NextFunction) {

        try {

            const { email, password } = req.body

            const user = await this.authService.authenticate(email, password)

            if (!user) {
                throw new NotFoundException('اطلاعات ورود صحیح نمی باشد!')
            }

            const token = TokenService.sign({
                uid: (user as IUser).id
            })

            res.send({
                success: true,
                message: 'ورود با موفقیت انجام شد',
                token
            })

        } catch (error) {
            next(error)
        }

    }

    public async register(req: Request, res: Response, next: NextFunction) {

        try {

            const { firstName, lastName, email, password } = req.body

            const registerResult = await this.authService.register(
                firstName,
                lastName,
                email,
                password
            )

            if (!registerResult) {
                throw new ServerException('فرآیند ثبت نام با خطایی مواجه شده است!')
            }

            res.send({
                success: true,
                message: 'ثبت نام با موفقیت انجام شد',
            })

        } catch (error) {
            next(error)
        }

    }

    public async check(req: Request, res: Response, next: NextFunction) {

        try {

            let token = req.headers.authorization

            if (!token) {
                throw new ValidationException('توکن نامعتبر!')
            }

            token = token.split(' ').pop() as string

            const decodedToken = TokenService.verify(token)

            if (!decodedToken) {
                throw new ValidationException('توکن نامعتبر!')
            }

            const { uid } = decodedToken as JwtPayload

            const user = await this.usersRepository.findOne(uid)

            if (!user) {
                throw new ValidationException('توکن نامعتبر!')
            }

            const transformUser = this.usersTransformer.transform(user)

            res.send({
                success: true,
                user: transformUser
            })

        } catch (error) {
            next(error)
        }

    }

}
