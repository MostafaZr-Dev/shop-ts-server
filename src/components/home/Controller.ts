import { NextFunction, Request, Response } from "express";

import ProductMongoRepository from "@components/products/repositories/ProductMongoRepository";
import IProductRepository from "@components/products/repositories/IProductRepository";
import ProductsTransformer from "@components/products/front/Transformer";
import ProductStatus from "@components/products/model/productStatus";

import NotFoundException from "@components/exceptions/NotFoundException";

class HomeController {

    private readonly productsRepository: IProductRepository;
    private readonly productsTransformer: ProductsTransformer;

    constructor() {
        this.list = this.list.bind(this)

        this.productsRepository = new ProductMongoRepository()
        this.productsTransformer = new ProductsTransformer()
    }

    public async list(req: Request, res: Response, next: NextFunction) {
        try {
            const newests = await this.productsRepository.findMany(
                {}, // filter by PUBLISHED status
                undefined,
                {
                    perPage: 3,
                    offset: 0
                },
                {
                    createdAt: -1
                }
            )

            const mostViewed = await this.productsRepository.findMany(
                {}, // filter by PUBLISHED status
                undefined,
                {
                    perPage: 3,
                    offset: 0
                },
                {
                    viewsCount: -1
                }
            )

            const populars = await this.productsRepository.findMany(
                {}, // filter by PUBLISHED status
                undefined,
                {
                    perPage: 3,
                    offset: 0
                },
                {
                    totalScore: -1
                }
            )

            const bestSellers = await this.productsRepository.findMany(
                {}, // filter by PUBLISHED status
                undefined,
                {
                    perPage: 3,
                    offset: 0
                },
                {
                    purchaseCount: -1
                }
            )

            res.send({
                newests: this.productsTransformer.collection(newests),
                bestSellers: this.productsTransformer.collection(bestSellers),
                populars: this.productsTransformer.collection(populars),
                mostViewed: this.productsTransformer.collection(mostViewed),
            })

        } catch (error) {
            next(error)
        }
    }

}

export default HomeController;