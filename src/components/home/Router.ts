import { Router } from 'express'

import HomeController from './Controller'

const homeControllerInstance = new HomeController()

const homeRouter: Router = Router()

homeRouter.get("/", homeControllerInstance.list)


export default homeRouter