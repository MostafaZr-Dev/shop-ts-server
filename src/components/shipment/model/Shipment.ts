import { Schema, model } from "mongoose";

import IShipment from "./IShipment";
import ShipmentStatus from "./ShipmentStatus";

const shipmentSchema: Schema = new Schema({

    employee: { type: String, required: true },
    order: { type: Schema.Types.ObjectId, ref: "orders" },
    selectedDateTime: { type: Date, required: true },
    deliveredAt: { type: Date, default: null },
    note: { type: String, default: null },
    status: { type: ShipmentStatus, required: true, default: ShipmentStatus.PENDING },
});


export default model<IShipment>("shipment", shipmentSchema)
