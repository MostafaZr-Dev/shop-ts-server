import { Schema, model } from "mongoose";
import IProductOffer from "./IProductOffer";

import productOfferItemSchema from "./ProductOfferItem";

const productOfferSchema: Schema = new Schema({

    products: { type: [productOfferItemSchema], required: true },
    startDate: { type: Date },
    endDate: { type: Date },
    createdAt: { type: Date, default: Date.now },

});

export default model<IProductOffer>("ProductOffer", productOfferSchema);
