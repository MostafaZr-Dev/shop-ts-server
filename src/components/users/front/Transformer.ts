import IUser from "../model/IUser"
import ITransformer from "@components/contracts/ITransformer"

export default class UsersTransformer implements ITransformer<IUser>{
    public transform(item: IUser) {

        return {
            id: item._id,
            fullName: `${item.firstName} ${item.lastName}`,
            email: item.email,
            mobile: item.mobile,
            totalOrders: item.totalOrders,
            wallet: item.wallet,
            addresses: item.addresses,
        }

    }

    public collection(items: IUser[]) {
        return items.map(item => this.transform(item))
    }

}