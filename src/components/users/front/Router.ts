import { Router } from "express";

import UsersController from "./Controller";
import { auth } from "@middlewares/auth";

const usersControllerInstance = new UsersController();

const usersRouter: Router = Router();

usersRouter.use(auth)

usersRouter.post("/:uid/addresses", usersControllerInstance.saveAddress);


export default usersRouter;