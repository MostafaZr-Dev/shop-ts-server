import ServerException from "@components/exceptions/ServerException";
import { NextFunction, Request, Response } from "express";
import IUserRepository from "../repositories/IUserRepository";
import UserMongoRepository from "../repositories/UserMongoRepository";


class UsersController {
    private readonly usersRepository: IUserRepository

    constructor() {
        this.saveAddress = this.saveAddress.bind(this)

        this.usersRepository = new UserMongoRepository()
    }

    public async saveAddress(req: Request, res: Response, next: NextFunction) {

        try {
            const {
                userID,
                title,
                fullName,
                state,
                city,
                mobile,
                zipCode,
                address
            } = req.body

            const user = await this.usersRepository.findOne(userID)

            let newAddresses = []

            if (user?.addresses) {
                newAddresses = [
                    ...user.addresses,
                    {
                        title,
                        fullName,
                        state,
                        city,
                        mobile,
                        zipCode,
                        address
                    }
                ]
            } else {
                newAddresses = [
                    {
                        title,
                        fullName,
                        state,
                        city,
                        mobile,
                        zipCode,
                        address
                    }
                ]
            }

            const result = await this.usersRepository.updateOne({
                _id: userID
            }, {
                addresses: newAddresses
            })

            if (!result) {
                throw new ServerException('امکان ذخیره سازی آدرس جدید در حال حاضر وجود ندارد!')
            }

            res.send({
                success: true,
                addresses: newAddresses
            })

        } catch (error) {
            next(error)
        }

    }

}

export default UsersController;