import { FilterQuery } from "mongoose";

import IPagination from "@components/contracts/IPagination";
import IUser from "../model/IUser";
import IUserRepository from "./IUserRepository";
import UserModel from '../model/User'


export default class UserMongoRepository implements IUserRepository {

    public async findByEmail(email: string): Promise<IUser | null> {
        return UserModel.findOne({
            email
        })
    }

    public async findOne(id: string): Promise<IUser | null> {
        return UserModel.findById(id)
    }

    public async findMany(params: any, relations?: string[], pagination?: IPagination): Promise<IUser[]> {
        return UserModel.find(params)
    }

    public async create(params: any): Promise<IUser> {
        const newUser = new UserModel({
            ...params
        })

        return newUser.save()
    }

    public async updateOne(where: Partial<IUser>, updateData: Partial<IUser>): Promise<boolean> {
        const updateResult = await UserModel.updateOne(where as FilterQuery<IUser>, updateData)

        if (updateResult.nModified > 0) {
            return true
        }

        return false
    }

    public async updateMany(where: Partial<IUser>, updateData: Partial<IUser>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(ID: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}