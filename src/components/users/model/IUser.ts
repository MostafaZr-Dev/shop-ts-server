import { Document } from "mongoose";

import IAddress from "./IAddress";

export default interface IUser extends Document {


    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    password: string;
    totalOrders: number;
    wallet: number;
    addresses: IAddress[];
    createAt: Date;

}