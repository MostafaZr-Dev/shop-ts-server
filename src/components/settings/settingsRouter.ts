import { Router } from 'express'
import { container } from "tsyringe";

import SettingsController from './settingsController'

const settingsControllerInstance = container.resolve(SettingsController)

const settingsRouter: Router = Router()

settingsRouter.get("/", settingsControllerInstance.index)
settingsRouter.post("/", settingsControllerInstance.store)

export default settingsRouter