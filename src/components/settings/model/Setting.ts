import { Schema, model } from "mongoose";

import ISetting from "./ISetting";
import SettingScope from "./SettingScope";

const settingSchema: Schema = new Schema({

    title: { type: String, required: true },
    key: { type: String, required: true },
    value: { type: String, required: true },
    scope: { type: SettingScope, required: true, default: SettingScope.PRIVATE },
    version: { type: String, required: true }

});


export default model<ISetting>("settings", settingSchema)