import { NextFunction, Request, Response } from "express";
import { autoInjectable } from "tsyringe";

import SettingMongoRepository from "./repositories/SettingMongoRepository";
import SettingsTransformer from "./settingsTransformer";

@autoInjectable()
export default class SettingsController {

    private readonly settingsRepository: SettingMongoRepository
    private readonly settingsTransformer: SettingsTransformer

    public constructor(
        settingsRepository: SettingMongoRepository,
        settingsTransformer: SettingsTransformer
    ) {
        this.index = this.index.bind(this)
        this.store = this.store.bind(this)

        this.settingsRepository = settingsRepository
        this.settingsTransformer = settingsTransformer
    }

    public async index(req: Request, res: Response, next: NextFunction): Promise<void> {

        try {
            const page = req.query.page || 1
            const perPage = 8
            const offset: number = (page as unknown as number - 1) * perPage

            const settings = await this.settingsRepository.findMany(
                {},
                undefined,
                {
                    perPage,
                    offset
                }
            )

            const totalSettings = await this.settingsRepository.findMany({})

            const transformedSettings = this.settingsTransformer.collection(settings)

            res.send({
                settings: transformedSettings,
                _metadata: {
                    page,
                    perPage,
                    totalItems: totalSettings.length,
                    totalPages: Math.ceil(totalSettings.length / perPage),
                }
            })
        } catch (error) {
            next(error)
        }

    }

    public async store(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {

            const settingData = {
                title: req.body.title,
                key: req.body.key,
                value: req.body.value,
                version: req.body.version,
                scope: req.body.scope
            }

            const newSetting = await this.settingsRepository.create(settingData)

            if (newSetting) {
                res.status(201).send({
                    success: 'true',
                    message: 'تنظیمات با موفقیت ایجاد شد'
                })
            }
        } catch (error) {
            next(error)
        }
    }


}