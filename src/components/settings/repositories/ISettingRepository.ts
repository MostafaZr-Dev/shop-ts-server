

import IRepository from "@components/contracts/IRepository";
import ISetting from "../model/ISetting";


export default interface ISettingRepository extends IRepository<ISetting> {

}