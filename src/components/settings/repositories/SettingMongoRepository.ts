import IPagination from "@components/contracts/IPagination";
import ISetting from "../model/ISetting";
import ISettingRepository from "./ISettingRepository";
import SettingModel from '../model/Setting'
import IUser from "@components/users/model/IUser";
import ObjectInterface from "@components/contracts/ObjectInterface";


export default class SettingMongoRepository implements ISettingRepository {
    public async findOne(id: string, relations?: string[]): Promise<ISetting | null> {
        throw new Error("Method not implemented.");
    }

    public async findMany(params: ObjectInterface, relations?: string[], pagination?: IPagination): Promise<ISetting[]> {
        const settingQueryParams: ObjectInterface = {}

        const settingQuery = SettingModel.find(settingQueryParams)

        if (relations && relations.length > 0) {

            relations.forEach((relation: string) => {
                settingQuery.populate(relation)
            })

        }

        if (pagination) {
            settingQuery.limit(pagination.perPage).skip(pagination.offset)
        }

        return settingQuery.exec()
    }

    public async create(params: any): Promise<ISetting> {
        const newProduct = new SettingModel({
            ...params
        })

        return newProduct.save()
    }

    public async updateOne(where: Partial<ISetting>, updateData: Partial<ISetting>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async updateMany(where: Partial<ISetting>, updateData: Partial<ISetting>): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(ID: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

}