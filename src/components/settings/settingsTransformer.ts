import ITransformer from "@components/contracts/ITransformer";
import ISetting from "./model/ISetting";

export default class SettingsTransformer implements ITransformer<ISetting>{
    public transform(item: ISetting) {

        return {
            id: item._id,
            title: item.title,
            key: item.key,
            value: item.value,
            version: item.version,
            scope: item.scope
        }

    }

    public collection(items: ISetting[]) {
        return items.map(item => this.transform(item))
    }

}