import { Request, Response } from "express";
import { autoInjectable } from 'tsyringe'

import UploadService from "@services/upload/uploadService";
import ProductMongoRepository from "../repositories/ProductMongoRepository";
import ProductsTransformer from "./Transformer";

@autoInjectable()
class ProductsController {

    private readonly uploadService: UploadService;
    private readonly productsRepository: ProductMongoRepository;
    private readonly productsTransformer: ProductsTransformer;

    constructor(
        uploadService: UploadService,
        productsRepository: ProductMongoRepository,
        productsTransformer: ProductsTransformer
    ) {
        this.index = this.index.bind(this)
        this.store = this.store.bind(this)

        this.uploadService = uploadService
        this.productsRepository = productsRepository
        this.productsTransformer = productsTransformer
    }

    public async index(req: Request, res: Response) {
        const page: number = parseInt(req.query.page as string) || 1
        const perPage = 10
        const offset: number = (page - 1) * perPage

        const products = await this.productsRepository.findMany(
            {},
            undefined,
            {
                perPage,
                offset
            }
        )

        const totalProducts = await this.productsRepository.findMany(
            {},
        )

        res.send({
            products: this.productsTransformer.collection(products),
            _metadata: {
                page,
                perPage,
                totalItems: totalProducts.length,
                totalPages: Math.ceil(totalProducts.length / perPage),
            }
        })
    }

    public async store(req: Request, res: Response) {
        const {
            title,
            price,
            discountedPrice,
            stock,
            category,
            attributes,
            variations,
            priceVariations,
            files
        } = req.body;

        const uploadedFiles = await this.uploadService.uploadFile(files, 'disk')

        const thumbnailName = uploadedFiles.filter(file => file.fieldName === 'thumbnail').pop()?.fileName
        const galleryFileNames = uploadedFiles.filter(file => file.fieldName === 'gallery').map(file => file.fileName)


        const newProductParams = {
            title,
            price,
            discountedPrice,
            stock,
            category,
            attributes: JSON.parse(attributes),
            variations: JSON.parse(variations),
            priceVariations: JSON.parse(priceVariations),
            thumbnail: thumbnailName,
            gallery: galleryFileNames
        }

        const newProduct = await this.productsRepository.create(newProductParams)


        res.send({
            newProduct
        })
    }

}

export default ProductsController;