import ITransformer from "@components/contracts/ITransformer";
import IProduct from "../model/IProduct";
import DateService from "@services/dateService"

export default class ProductsTransformer implements ITransformer<IProduct>{
    public transform(item: IProduct) {

        return {
            id: item._id,
            title: item.title,
            thumbnail: item.thumbnailUrl,
            gallery: item.galleryUrls,
            price: item.price,
            stock: item.stock,
            discountedPrice: item.discountedPrice,
            createdAt: DateService.toPersian(item.createdAt.toUTCString()),
            updatedAt: DateService.toPersian(item.updatedAt.toUTCString()),
            status: item.status
        }

    }

    public collection(items: IProduct[]) {
        return items.map(item => this.transform(item))
    }

}