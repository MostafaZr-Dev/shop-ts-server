import { Router } from 'express'
import { container } from 'tsyringe'

import ProductsController from './Controller'
import multer from '@middlewares/upload/multer'
import fileMapper from '@middlewares/upload/fileMapper'

const productsControllerInstance = container.resolve(ProductsController)

const productsRouter: Router = Router()

productsRouter.get("/", productsControllerInstance.index)
productsRouter.post("/",
    multer([
        { name: "thumbnail", maxCount: 1 },
        { name: "gallery", maxCount: 5 }
    ]),
    fileMapper,
    productsControllerInstance.store
)

export default productsRouter