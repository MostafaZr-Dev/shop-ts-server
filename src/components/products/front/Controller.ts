import { NextFunction, Request, Response } from "express";
import { autoInjectable } from 'tsyringe'

import ProductMongoRepository from "../repositories/ProductMongoRepository";
import CommentMongoRepository from "@components/comments/repositories/CommentMongoRepository";
import ProductsTransformer from "./Transformer";
import CommentsTransformer from "@components/comments/front/Transformer";

import NotFoundException from "@components/exceptions/NotFoundException";

@autoInjectable()
class ProductsController {

    private readonly productsRepository: ProductMongoRepository;
    private readonly commentsRepository: CommentMongoRepository;
    private readonly productsTransformer: ProductsTransformer;
    private readonly commentsTransformer: CommentsTransformer;

    constructor(
        productsRepository: ProductMongoRepository,
        commentsRepository: CommentMongoRepository,
        productsTransformer: ProductsTransformer,
        commentsTransformer: CommentsTransformer
    ) {
        this.list = this.list.bind(this)
        this.product = this.product.bind(this)
        this.comments = this.comments.bind(this)

        this.productsRepository = productsRepository
        this.commentsRepository = commentsRepository
        this.productsTransformer = productsTransformer
        this.commentsTransformer = commentsTransformer
    }

    public async list(req: Request, res: Response, next: NextFunction) {
        try {
            const page = req.query.page || 1
            const perPage = 12
            const offset: number = (page as unknown as number - 1) * perPage

            const products = await this.productsRepository.findMany(
                {},
                undefined,
                {
                    perPage,
                    offset
                }
            )

            const totalProducts = await this.productsRepository.findMany({})

            res.send({
                products: this.productsTransformer.collection(products),
                _metadata: {
                    page,
                    perPage,
                    totalItems: totalProducts.length,
                    totalPages: Math.ceil(totalProducts.length / perPage),
                }
            })
        } catch (error) {
            next(error)
        }
    }

    public async product(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params

            const product = await this.productsRepository.findOne(
                id,
                ['category']
            )

            if (!product) {
                throw new NotFoundException('!محصول یافت نشد')
            }

            const transformedProduct = this.productsTransformer.transform(product)

            res.send({
                product: transformedProduct
            })

        } catch (error) {
            next(error)
        }
    }

    public async comments(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params

            const comments = await this.commentsRepository.findByProductID(
                id,
                ['user']
            )

            const transformedComments = this.commentsTransformer.collection(comments)

            res.send({
                comments: transformedComments
            })

        } catch (error) {
            next(error)
        }
    }

}

export default ProductsController;