import { Router } from 'express'
import { container } from 'tsyringe'

import ProductsController from './Controller'

const productsControllerInstance = container.resolve(ProductsController)

const productsRouter: Router = Router()

productsRouter.get("/", productsControllerInstance.list)
productsRouter.get("/:id", productsControllerInstance.product)
productsRouter.get("/:id/comments", productsControllerInstance.comments)

export default productsRouter