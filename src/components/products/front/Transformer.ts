import ITransformer from "@components/contracts/ITransformer";
import IProduct from "../model/IProduct";
import DateService from "@services/dateService"

export default class ProductsTransformer implements ITransformer<IProduct>{
    public transform(item: IProduct) {

        return {
            id: item._id,
            title: item.title,
            slug: item.slug,
            category: item.category,
            thumbnail: item.thumbnail,
            gallery: item.gallery,
            price: item.price,
            stock: item.stock,
            variations: item.variations,
            priceVariations: item.priceVariations,
            discountedPrice: item.discountedPrice,
            attributes: item.attributes,
            createdAt: DateService.toPersian(item.createdAt.toUTCString()),
            updatedAt: DateService.toPersian(item.updatedAt.toUTCString()),
            status: item.status
        }

    }

    public collection(items: IProduct[]) {
        return items.map(item => this.transform(item))
    }

}