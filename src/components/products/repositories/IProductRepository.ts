import IRepository from "@components/contracts/IRepository";
import IProduct from "../model/IProduct";
import ProductStatus from "../model/ProductStatus";


export default interface IProductRepository extends IRepository<IProduct> {

    findByStatus(status: ProductStatus): Promise<IProduct[]>

}