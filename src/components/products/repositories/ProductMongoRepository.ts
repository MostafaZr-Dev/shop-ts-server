import { Types } from 'mongoose'

import IProductRepository from "./IProductRepository";
import ProductModel from "../model/Product";
import ProductStatus from "../model/ProductStatus";
import IProduct from "../model/IProduct";
import IPagination from "@components/contracts/IPagination";
import ObjectInterface from "@components/contracts/ObjectInterface";



export default class ProductMongoRepository implements IProductRepository {

    public async findByStatus(status: ProductStatus): Promise<IProduct[]> {
        return ProductModel.find({ status })
    }

    public async findOne(ID: string, relations?: string[]): Promise<IProduct | null> {
        const productQuery = ProductModel.findById(ID)

        if (relations) {
            relations.forEach((relation: string) => {
                productQuery.populate(relation)
            })
        }

        return productQuery.exec()
    }

    public async findMany(params: any, relations?: string[], pagination?: IPagination, sort?: any): Promise<IProduct[]> {
        const productQueryParams: ObjectInterface = { ...params }

        if (params.category) {
            const objectID = Types.ObjectId
            productQueryParams.category = new objectID(params.category)
        }

        const productQuery = ProductModel.find(productQueryParams)

        if (sort) {
            productQuery.sort(sort)
        }

        if (relations) {
            relations.forEach((relation: string) => {
                productQuery.populate(relation)
            })
        }

        if (pagination) {
            productQuery.limit(pagination.perPage).skip(pagination.offset)
        }

        return productQuery.exec()
    }

    public async create(params: any): Promise<IProduct> {
        const newProduct = new ProductModel({
            ...params
        })

        return newProduct.save()
    }

    public async updateOne(where: Partial<IProduct>, updateData: Partial<IProduct>): Promise<boolean> {
        // return ProductModel.updateOne(where, updateData)
        throw new Error("Method not implemented.");
    }

    public async updateMany(where: any, updateData: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    public async deleteOne(where: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    public async deleteMany(where: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}