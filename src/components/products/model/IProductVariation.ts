export interface IProductVariationItem {
    title:string
    value:string
}



export default interface IProductVariation {

    title: string;
    type: string;
    name: string;
    items: IProductVariationItem[]

}