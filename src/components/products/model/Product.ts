import { Schema, model } from "mongoose";

import IProduct from "./IProduct";
import ProductStatus from "./ProductStatus";


const productSchema: Schema = new Schema({

    title: { type: String, required: true },
    slug: { type: String, required: true },
    price: { type: Number, required: true },
    discountedPrice: { type: Number, default: 0 },
    stock: { type: Number },
    thumbnail: { type: String },
    gallery: { type: [String] },
    category: { type: Schema.Types.ObjectId, ref: "categories" },
    attributes: { type: [Object], required: true },
    variations: { type: [Object] },
    priceVariations: { type: [Object] },
    purchaseCount: { type: Number, default: 0 },
    commentsCount: { type: Number, default: 0 },
    totalScore: { type: Number, default: 0 },
    viewsCount: { type: Number, default: 0 },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    status: { type: ProductStatus, default: ProductStatus.INIT }

});

productSchema.set('toJSON', {
    virtuals: true
})

productSchema.virtual('thumbnailUrl').get(function (this: IProduct) {
    return `${process.env.APP_URL}/contents/media/${this.thumbnail}`
})

productSchema.virtual('galleryUrls').get(function (this: IProduct) {
    return this.gallery?.map((fileName: string) => `${process.env.APP_URL}/contents/media/${fileName}`)
})


export default model<IProduct>("products", productSchema)