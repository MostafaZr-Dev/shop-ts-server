
interface IProductAttribute {

    title: string;
    slug: string;
    value: string;
    filterable: boolean;
    hasPrice: boolean;


}

export default IProductAttribute