import { Document } from "mongoose";

import IAttributeGroup from "./IAttributeGroup";
import IPriceVariation from "./IPriceVariation";
import IProductVariation from "./IProductVariation";
import ProductStatus from "./ProductStatus";

export default interface IProduct extends Document {

    title: string;
    slug: string;
    price: number;
    discountedPrice: number;
    stock: number;
    thumbnail?: string;
    gallery?: string[];
    thumbnailUrl?: string;
    galleryUrls?: string[];
    category: string;
    attributes: IAttributeGroup[];
    variations: IProductVariation[];
    priceVariations: IPriceVariation[];
    purchaseCount: number;
    commentsCount: number;
    totalScore: number;
    viewsCount: number;
    createdAt: Date;
    updatedAt: Date;
    status: ProductStatus;


}