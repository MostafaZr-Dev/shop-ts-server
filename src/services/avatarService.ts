import * as crypto from 'crypto'

const gravatarUrl = 'https://www.gravatar.com/avatar'

export const buildAvatar = (email: string, size: number = 50): string => {
    const hashedEmail: string = crypto.createHash('md5').update(email).digest('hex')

    return `${gravatarUrl}/${hashedEmail}?s=${size}`
}