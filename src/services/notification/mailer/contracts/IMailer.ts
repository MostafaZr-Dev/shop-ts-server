import IMailMessage from "./IMailMessage";

export default interface IMailer {

    send(mail: IMailMessage): void

}