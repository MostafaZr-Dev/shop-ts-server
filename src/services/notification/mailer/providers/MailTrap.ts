import { createTransport, Transporter } from "nodemailer";

import IMailer from "../contracts/IMailer";
import IMailMessage from "../contracts/IMailMessage";


export default class MailTrap implements IMailer {
    private readonly smtpTransport: Transporter
    constructor() {
        //read from env
        this.smtpTransport = createTransport({
            host: "smtp.mailtrap.io",
            port: 2525,
            auth: {
                user: "99f32db0927fdb",
                pass: "78d18a810b8887"
            }
        })
    }

    public async send(message: IMailMessage): Promise<void> {

        this.smtpTransport.sendMail({
            from: 'shop@gmail.com',
            to: message.receipent,
            subject: message.subject,
            text: message.body
        })

    }

}