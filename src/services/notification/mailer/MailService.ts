import IMailer from "./contracts/IMailer";
import IMailMessage from "./contracts/IMailMessage";
import MailTrap from "./providers/MailTrap";

export default class MailService {
    private readonly defaultProvider: IMailer

    constructor() {
        this.defaultProvider = new MailTrap()
    }

    public async send(message: IMailMessage): Promise<void> {
        if (message.receipent !== '') {
            this.defaultProvider.send(message)
        }
    }

}