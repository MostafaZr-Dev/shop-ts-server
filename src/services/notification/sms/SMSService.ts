import ISMSMessage from "./contracts/ISMSMessage";
import ISMSProvider from "./contracts/ISMSProvider";
import SibSMS from "./providers/SibSMS";

export default class SMSService {
    private readonly defaultProvider: ISMSProvider

    constructor() {
        this.defaultProvider = new SibSMS()
    }

    public async send(message: ISMSMessage) {
        if (message.to !== '') {
            this.defaultProvider.send(message)
        }
    }

}