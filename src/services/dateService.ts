import * as moment from "jalali-moment"

moment.locale("fa")

export default class DateService {

    public static toPersian(input: string, format:string = 'YYYY/M/D HH:mm:ss'): string {

        return moment(input).format(format)

    }

}