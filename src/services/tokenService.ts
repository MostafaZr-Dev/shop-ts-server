import * as jwt from 'jsonwebtoken'

export default class TokenService {

    public static sign(data: any) {
        return jwt.sign(data, process.env.APP_SECRET as string)
    }

    public static verify(token: string) {
        try {
            return jwt.verify(token, process.env.APP_SECRET as string)
        } catch (error) {
            return false
        }
    }

}