import { v4 as uuid } from "uuid";
import { hashSync, compareSync } from 'bcrypt'

export default class HashService {

    public static generateID(): string {
        return uuid()
    }

    public static hashPassword(plainPassword: string): string {
        return hashSync(plainPassword, 10)
    }

    public static comparePassword(hashedPassword: string, plainPassword: string): boolean {
        return compareSync(plainPassword, hashedPassword)
    }



}