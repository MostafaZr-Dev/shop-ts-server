import * as ZarinpalCheckout from 'zarinpal-checkout'

import IOnlineGateway from "../contracts/IOnlineGateway";
import IPaymentRequest from '../contracts/IPaymentRequest';
import IPaymentVerify from '../contracts/IPaymentVerify';

export default class ZarinPalGateway implements IOnlineGateway {

    private readonly sandbox: boolean
    private readonly merchantID: string
    private readonly zarinpal: ZarinpalCheckout.ZarinPalInstance

    public constructor() {
        console.log(process.env.ZARIN_SANDBOX, process.env.ZARIN_MERCHANT)
        this.merchantID = process.env.ZARIN_MERCHANT as string
        this.sandbox = Number(process.env.ZARIN_SANDBOX) ? true : false
        this.zarinpal = ZarinpalCheckout.create('xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx', true)
    }

    public async paymentRequest(request: IPaymentRequest): Promise<any> {
        const appFrontURl: string = process.env.APP_FRONT_URL as string

        const requestResult = await this.zarinpal.PaymentRequest({
            Amount: request.amount, // In Tomans
            CallbackURL: `${appFrontURl}/payment/verify/${request.reserve}`,
            Description: request.description,
        })

        if (requestResult && requestResult.status === 100) {
            return {
                success: true,
                url: requestResult.url
            }
        }

        return {
            success: false
        }
    }

    public async paymentVerify(verify: IPaymentVerify): Promise<any> {
        const verifyResult = await this.zarinpal.PaymentVerification({
            Amount: verify.amount, // In Tomans
            Authority: verify.refID,
        })

        if (verifyResult && verifyResult.status === -21) {
            return {
                success: false
            }
        }

        return {
            success: true,
            refID: verifyResult.RefID
        }
    }
}