import IPaymentMethod from "./contracts/IPaymentMethod";
import CODPayment from "./methods/CODPayment";
import OnlinePayment from "./methods/OnlinePayment";

export default class PaymentMethodFactory {

    private methods: Map<string, IPaymentMethod> = new Map<string, IPaymentMethod>()

    constructor() {

        this.methods.set('cod', new CODPayment())
        this.methods.set('online', new OnlinePayment())

    }


    public make(method: string): IPaymentMethod {

        if (!this.methods.has(method)) {
            throw new Error('روش پرداخت نامعتبر می باشد!')
        }

        return this.methods.get(method) as IPaymentMethod
    }
}