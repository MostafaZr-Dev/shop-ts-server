import { autoInjectable } from 'tsyringe'

import NotFoundException from "@components/exceptions/NotFoundException";
import IOrder from "@components/orders/model/IOrder";
import PaymentMongoRepository from "@components/payment/repositories/PaymentMongoRepository";
import HashService from "@services/hashService";
import OnlinePayment from "./methods/OnlinePayment";
import PaymentMethodFactory from "./PaymentMethodFactory";

@autoInjectable()
export default class PaymentService {

    private readonly paymentRepository: PaymentMongoRepository
    private readonly paymentMethodFactory: PaymentMethodFactory
    constructor(
        paymentRepository: PaymentMongoRepository,
        paymentMethodFactory: PaymentMethodFactory
    ) {

        this.paymentRepository = paymentRepository
        this.paymentMethodFactory = paymentMethodFactory
    }

    public async payOrder(order: IOrder, method: string, gateway: string): Promise<any> {

        const newPayment = await this.paymentRepository.create({
            user: order.user,
            order: order._id,
            amount: order.finalPrice,
            method,
            gateway,
            reserve: HashService.generateID()
        })

        const paymentProvider = this.paymentMethodFactory.make(method)

        if (paymentProvider instanceof OnlinePayment) {
            paymentProvider.setGetway(gateway)
        }

        return paymentProvider.doPayment(newPayment)

    }

    public async verifyPayment(peymentVerifyData: any): Promise<{ success: boolean, refID?: string }> {

        const payment = await this.paymentRepository.findByReserve(peymentVerifyData.reserve)

        if (!payment) {
            throw new NotFoundException('هیچ پرداختی با این شناسه وجود ندارد!')
        }

        const paymentProvider = this.paymentMethodFactory.make(payment.method)

        if (paymentProvider instanceof OnlinePayment) {
            paymentProvider.setGetway(payment.gateway)
            return paymentProvider.verifyPayment({ ...peymentVerifyData, amount: payment.amount })
        }


        return {
            success: false
        }
    }

}