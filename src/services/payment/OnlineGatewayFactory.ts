import IOnlineGateway from "./contracts/IOnlineGateway";
import ZarinPalGateway from "./online/ZarinPalGateway";

export default class OnlineGatewayFactory {
    private gateways: Map<string, IOnlineGateway> = new Map<string, IOnlineGateway>()

    constructor() {

        this.gateways.set('zarinpal', new ZarinPalGateway())
    }

    public make(gateway: string): IOnlineGateway {
        if(!this.gateways.has(gateway)){
            throw new Error('درگاه نامعتبر است!')
        }
        return this.gateways.get(gateway) as IOnlineGateway
    }

}