import IPayment from "@components/payment/model/IPayment";
import IPaymentMethod from "../contracts/IPaymentMethod";
import IPaymentRequest from "../contracts/IPaymentRequest";
import IPaymentVerify from "../contracts/IPaymentVerify";
import OnlineGatewayFactory from "../OnlineGatewayFactory";


export default class OnlinePayment implements IPaymentMethod {

    private gateway: string = ''
    private readonly onlineGatewayFactory: OnlineGatewayFactory

    constructor() {
        this.onlineGatewayFactory = new OnlineGatewayFactory()
    }

    public setGetway(gateway: string) {
        this.gateway = gateway
    }

    public async doPayment(payment: IPayment): Promise<any> {
        const onlineGateway = this.onlineGatewayFactory.make(this.gateway)

        const paymentRequest: IPaymentRequest = {
            amount: payment.amount,
            description: `بابت پرداخت سفارش با شناسه ${payment.order}`,
            reserve: payment.reserve
        }

        return onlineGateway.paymentRequest(paymentRequest)
    }

    public async verifyPayment(clientPaymentData: any): Promise<{ success: boolean, refID?: string }> {
        const onlineGateway = this.onlineGatewayFactory.make(this.gateway)

        const paymentVerifyData: IPaymentVerify = {
            amount: clientPaymentData.amount,
            refID: clientPaymentData.authority,
            status: clientPaymentData.status
        }

        return onlineGateway.paymentVerify(paymentVerifyData)
    }

}