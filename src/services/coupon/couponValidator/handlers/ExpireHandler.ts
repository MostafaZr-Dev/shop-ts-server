import AbstractCouponHandler from "../AbstractCouponHandler";
import ICoupon from "@components/coupon/model/ICoupon";
import IUser from "@components/users/model/IUser";
import ValidationException from "@components/exceptions/ValidationException";

class ExpireHandler extends AbstractCouponHandler {
    public process(user: IUser | null, coupon: ICoupon): ICoupon | null {

        const now = new Date();

        if (now > coupon.expiresAt) {
            throw new ValidationException("مدت زمان استفاده از کد تخفیف به پایان رسیده است!");
        }

        return super.process(user, coupon);

    }
}

export default ExpireHandler;