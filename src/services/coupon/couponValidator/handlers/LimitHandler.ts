import ICoupon from "@components/coupon/model/ICoupon";
import ValidationException from "@components/exceptions/ValidationException";
import IUser from "@components/users/model/IUser";
import AbstractCouponHandler from "../AbstractCouponHandler";


class LimitHandler extends AbstractCouponHandler {

    public process(user: IUser | null, coupon: ICoupon): ICoupon | null {

        if (coupon.used >= coupon.limit) {
            throw new ValidationException("کد تخفیف معتبر نیست!");
        }

        return super.process(user, coupon);

    }

}

export default LimitHandler;