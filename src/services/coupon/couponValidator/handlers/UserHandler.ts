import ICoupon from "@components/coupon/model/ICoupon";
import ValidationException from "@components/exceptions/ValidationException";
import IUser from "@components/users/model/IUser";
import AbstractCouponHandler from "../AbstractCouponHandler";


class UserHandler extends AbstractCouponHandler {

    public process(user: IUser | null, coupon: ICoupon): ICoupon | null {

        if (coupon.constraints && 'users' in coupon.constraints) {
            const { users } = coupon.constraints;

            if (user && users.length > 0 && !users.includes(user.id)) {
                throw new ValidationException("این کد تخفیف برای کاربری شما صادر نشده است!");
            }
        }

        return super.process(user, coupon);

    }

}

export default UserHandler;