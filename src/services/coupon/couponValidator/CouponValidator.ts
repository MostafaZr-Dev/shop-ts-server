import ICoupon from "@components/coupon/model/ICoupon";
import IUser from "@components/users/model/IUser";

import ExpireHandler from "./handlers/ExpireHandler";
import LimitHandler from "./handlers/LimitHandler";
import UserHandler from "./handlers/UserHandler";


class CouponValidator {

    public handle(user: IUser | null, coupon: ICoupon) {

        const userHandler = new UserHandler();
        const limitHandler = new LimitHandler();
        const expireHandler = new ExpireHandler();

        expireHandler.setNext(userHandler).setNext(limitHandler);

        return expireHandler.process(user, coupon);

    }

}

export default CouponValidator;