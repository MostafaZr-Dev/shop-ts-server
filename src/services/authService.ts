import HashService from "./hashService";
import IUserRepository from '@components/users/repositories/IUserRepository'
import UserMongoRepository from '@components/users/repositories/UserMongoRepository'
import IUser from '@components/users/model/IUser'


export default class AuthService {
    private readonly usersRepository: IUserRepository
    constructor() {
        this.usersRepository = new UserMongoRepository()
    }

    public async authenticate(email: string, password: string): Promise<IUser | boolean> {

        const user = await this.usersRepository.findByEmail(email)

        if (!user) {
            return false
        }

        const isValidPassword = HashService.comparePassword(user.password, password)

        if (isValidPassword) {
            return user
        }

        return false
    }

    public async register(firstName: string, lastName: string, email: string, password: string) {
        const hashedPassword = HashService.hashPassword(password)

        const newUser = await this.usersRepository.create({
            firstName,
            lastName,
            email,
            password: hashedPassword
        })

        if (!newUser) {
            return false
        }

        return true
    }

}