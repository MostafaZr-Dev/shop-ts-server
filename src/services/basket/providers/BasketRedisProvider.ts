import IProduct from "@components/products/model/IProduct";
import IBasket from "../contracts/IBasket";

import redisConnection from "@infrastructure/connection/redis";
import IBasketConfigurable from "../contracts/IBasketConfigurable";


class BasketRedisProvider implements IBasket, IBasketConfigurable {

    private key: string = '';

    public config(config: string) {
        this.key = config;
    }

    public add(product: IProduct): void {
        redisConnection.get(this.key)
            .then(result => {
                if (result) {

                    const items = JSON.parse(result as string);

                    items.push(product);

                    redisConnection.set(this.key, JSON.stringify(items))
                        .then(result => console.log(result))
                        .catch(err => console.log(err));

                }
            })
            .catch(err => console.log('redis can not fetch basket items!', err.message));
    }

    public remove(product: IProduct): void {
        redisConnection.get(this.key)
            .then(result => {
                if (result) {

                    const items = JSON.parse(result as string);

                    items.splice(items.indexOf(product), 1);

                    redisConnection.set(this.key, JSON.stringify(items))
                        .then(result => console.log(result))
                        .catch(err => console.log(err));

                }
            })
            .catch(err => console.log('redis can not fetch basket items!', err.message));
    }

    public async items(): Promise<IProduct[]> {
        const basketItems = await this.getItems();

        return basketItems;
    }

    public async count(): Promise<number> {
        const basketItems = await this.getItems();

        return basketItems.length;
    }

    public clear(): void {
        redisConnection.del(this.key);
    }

    public async total(): Promise<number> {
        const basketItems = await this.getItems();

        return basketItems.reduce((total: number, product: IProduct) => {
            return total + product.price;
        }, 0);
    }

    public async has(product: IProduct): Promise<boolean> {
        const basketItems = await this.getItems();

        return basketItems.includes(product);
    }

    private async getItems(): Promise<IProduct[]> {
        const basketItems = await redisConnection.get(this.key)
            .then(result => result)
            .catch(err => false);

        if (basketItems) {
            const decodedItems = JSON.parse(basketItems as string);
            return decodedItems;
        }

        return [];
    }

}

export default BasketRedisProvider;