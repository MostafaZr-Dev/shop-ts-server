import IUpload from './contracts/IUpload'
import DiskStorage from './storages/DiskStorage'


export default class UploadFactory {
    private storages: Map<string, IUpload> = new Map<string, IUpload>()


    public constructor() {
        this.storages.set('disk', new DiskStorage())
    }

    public make(storage: string): IUpload {
        if (!this.storages.has(storage)) {
            throw new Error('روش آپلود نامعتبر است!');
        }
        return this.storages.get(storage) as IUpload
    }


}