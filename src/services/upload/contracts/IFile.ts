export default interface IFile {
    fieldName: string;
    name: string;
    type: string;
    size: number;
    buffer: Buffer;
    extension: string;
}