import IFile from './IFile'
import IUploadedFile from './IUploadedFile'

export default interface IUpload {

    upload(files: IFile[]): Promise<IUploadedFile[]>;

}