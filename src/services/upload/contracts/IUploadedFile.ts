import IFile from "./IFile"

export default interface IUploadedFile extends IFile {
    path: string;
    fileName: string;
}