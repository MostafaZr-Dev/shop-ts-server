import * as fs from 'fs'
import { Readable } from 'stream'
import * as path from 'path'

import IFile from '../contracts/IFile'
import IUpload from '../contracts/IUpload'
import IUploadedFile from '../contracts/IUploadedFile'
import hashService from '@services/hashService'

export default class DiskUpload implements IUpload {

    private readonly uploadDir: string
    private readonly mediaDir: string

    public constructor() {
        this.uploadDir = path.join(process.cwd(), "/public/contents")
        this.mediaDir = `${this.uploadDir}/media`
    }

    public async upload(files: IFile[]): Promise<IUploadedFile[]> {

        const uploadedFiles = await Promise.all(files.map((file: IFile) => {

            const readStream = Readable.from(file.buffer)

            const path = this.getPath(file)

            const outputStream = fs.createWriteStream(path);

            readStream.pipe(outputStream)

            return new Promise<IUploadedFile>((resolve, reject) => {
                outputStream.on("error", reject);

                outputStream.on("finish", async () => {

                    resolve({
                        ...file,
                        path,
                        fileName: this.getFileName(path)
                    });
                });
            })
        }))

        return uploadedFiles
    }

    private getPath(file: IFile): string {

        if (!this.existDir(this.mediaDir)) {
            this.makeDir(this.mediaDir, { recursive: true });
        }

        const fileName = this.generateFileName(file)

        return `${this.mediaDir}/${fileName}`
    }

    private existDir(path: string): boolean {
        return fs.existsSync(path)
    }

    private makeDir(path: string, options?: object) {
        fs.mkdirSync(path, options);
    }

    private getFileName(path: string): string {
        return path.split("/").pop() as string
    }

    private generateFileName(file: IFile) {
        return `${hashService.generateID()}.${file.extension}`;
    }

}