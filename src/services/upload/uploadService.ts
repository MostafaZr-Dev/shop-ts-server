import { autoInjectable } from 'tsyringe'

import IFile from "./contracts/IFile";
import IUploadedFile from "./contracts/IUploadedFile";
import UploadFactory from "./uploadFactory";

@autoInjectable()
class UploadService {

    private uploadStorageFactory: UploadFactory

    constructor(
        uploadStorageFactory: UploadFactory
    ) {
        this.uploadStorageFactory = uploadStorageFactory
    }

    public async uploadFile(files: IFile[], storage: string): Promise<IUploadedFile[]> {
        const uploadStorage = this.uploadStorageFactory.make(storage)

        return uploadStorage.upload(files)
    }


}

export default UploadService