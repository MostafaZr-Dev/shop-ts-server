import * as express from "express"
import { Application } from "express"
import * as cors from "cors"
import { resolve } from "path"

export default function boot(app: Application) {
    app.use(cors());
    app.use(express.json());
    app.use(express.static(resolve(process.cwd(), 'public')))
}