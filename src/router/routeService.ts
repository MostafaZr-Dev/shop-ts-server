import { Application, Router } from "express";

import RouteEngine from "./router";

//admin
import usersAdminRouter from "@components/users/admin/Router";
import couponsAdminRouter from "@components/coupon/admin/Router";
import productsAdminRouter from "@components/products/admin/Router";
import paymentsAdminRouter from "@components/payment/admin/Router";
import ordersAdminRouter from "@components/orders/admin/Router";
import categoryAdminRouter from "@components/category/admin/categoryRouter";
import settingsRouter from "@components/settings/settingsRouter";
//front
import productsFrontRouter from "@components/products/front/Router";
import couponsFrontRouter from "@components/coupon/front/Router";
import paymentsFrontRouter from "@components/payment/front/Router";
import ordersFrontRouter from "@components/orders/front/Router";
import categoryFrontRouter from "@components/category/front/Router";
import authRouter from "@components/auth/authRouter";
import usersFrontRouter from "@components/users/front/Router";
import purchaseFrontRouter from "@components/purchase/Router";
import homeRouter from "@components/home/router";

class RouteService {
    private app: Application;
    private router: RouteEngine;

    public constructor(app: Application) {
        this.app = app;
        this.router = new RouteEngine();
        this.bindRouters();
    }

    public bindRouters() {
        //admin
        this.router.registerRouter('/api/v1/admin/products', productsAdminRouter)
        this.router.registerRouter('/api/v1/admin/coupons', couponsAdminRouter)
        this.router.registerRouter('/api/v1/admin/users', usersAdminRouter)
        this.router.registerRouter('/api/v1/admin/payments', paymentsAdminRouter)
        this.router.registerRouter('/api/v1/admin/orders', ordersAdminRouter)
        this.router.registerRouter('/api/v1/admin/categories', categoryAdminRouter)
        this.router.registerRouter('/api/v1/settings', settingsRouter)
        //front
        this.router.registerRouter('/api/v1/products', productsFrontRouter)
        this.router.registerRouter('/api/v1/coupons', couponsFrontRouter)
        this.router.registerRouter('/api/v1/payments', paymentsFrontRouter)
        this.router.registerRouter('/api/v1/orders', ordersFrontRouter)
        this.router.registerRouter('/api/v1/purchase', purchaseFrontRouter)
        this.router.registerRouter('/api/v1/categories', categoryFrontRouter)
        this.router.registerRouter('/api/v1/auth', authRouter)
        this.router.registerRouter('/api/v1/users', usersFrontRouter)
        this.router.registerRouter('/api/v1/home', homeRouter)

    }

    public run() {
        this.router.getRouters().forEach((router: Router, route: string) => {
            this.app.use(route, router);
        });
    }

}

export default RouteService;