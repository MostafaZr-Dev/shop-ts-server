import { Application } from "express";

import ExceptionHandler from "./ExceptionHandler";
import NotFoundHandler from "./NotFoundHandler";

export default function (app: Application) {

    app.use(ExceptionHandler)
    app.use(NotFoundHandler)

}