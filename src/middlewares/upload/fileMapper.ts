import IFile from "@services/upload/contracts/IFile";
import { NextFunction, Request, Response } from "express";



export default (req: Request, res: Response, next: NextFunction) => {

    const reqFiles = req.files;

    const filesValues = Object.values(reqFiles as {
        [fieldname: string]: Express.Multer.File[];
    })
    const files = filesValues.reduce((files, current) => files.concat([...current]), [])

    const mappedFiles: IFile[] = ((files) || []).map((file) => {
        return {
            fieldName: file.fieldname,
            name: file.originalname,
            type: file.mimetype,
            buffer: file.buffer,
            size: file.size,
            extension: `${file.originalname.split(".").pop()}`,
        }
    })

    Object.assign(req.body, { files: mappedFiles })

    return next()
}