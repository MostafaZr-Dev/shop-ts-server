import * as multer from "multer";


const upload = (fields: multer.Field[]) => {

    return multer().fields(fields)

}


export default upload