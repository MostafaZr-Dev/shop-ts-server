import { Request, NextFunction, Response } from "express";

import UnauthorizedException from "@components/exceptions/UnAuthorizedException";
import TokenService from "@services/tokenService";
import { JwtPayload } from "jsonwebtoken";

export const auth = (req: Request, res: Response, next: NextFunction) => {
    try {

        let token = req.headers?.authorization

        if (!token) {
            throw new UnauthorizedException('unauthorized!')
        }

        token = token.split(' ').pop()

        if (!token) {
            throw new UnauthorizedException('unauthorized!')
        }

        const verifyResult = TokenService.verify(token)

        if (!verifyResult) {
            throw new UnauthorizedException('unauthorized!')
        }

        Object.assign(req.body , { userID: (verifyResult as JwtPayload).uid })

        next()
    } catch (error) {
        next(error)
    }
}