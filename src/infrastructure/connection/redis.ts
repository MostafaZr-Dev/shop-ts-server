import { Tedis } from "tedis";

const { REDIS_HOST, REDIS_PORT } = process.env;

const redisConnection = new Tedis({
    port: REDIS_PORT as unknown as number,
    host: REDIS_HOST
});

export default redisConnection;